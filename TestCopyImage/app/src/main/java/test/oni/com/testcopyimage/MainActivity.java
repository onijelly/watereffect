package test.oni.com.testcopyimage;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.media.Image;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;
import android.widget.ImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ImageView img=(ImageView)findViewById(R.id.my_image);
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = false;
        Bitmap photo = BitmapFactory.decodeResource(getResources(), R.drawable.dragonegg, options);
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;

        Bitmap draw = Bitmap.createBitmap(photo.getWidth(),height, Bitmap.Config.ARGB_8888);
        Log.i("Height","draw image x: "+draw.getWidth()+" - "+draw.getHeight());
        Log.i("Size","Photo size: "+photo.getWidth()+" - "+photo.getHeight());
        for(int i=0; i<draw.getWidth();i++){
            for(int j=0; j<draw.getHeight(); j++){
                draw.setPixel(i,j, Color.BLACK);
            }
        }
        for(int i=0; i<photo.getWidth();i++){
            for(int j=0; j<photo.getHeight();j++){
                draw.setPixel(i,j,photo.getPixel(i,j));
            }
        }

        try {
            img.setImageURI(getDrawURI(draw));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Uri getDrawURI(Bitmap bmp) throws Exception{
        //create a file to write bitmap data
        File f = new File(getCacheDir(), "after_crop");
        f.createNewFile();
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
        byte[] bitmapdata = bos.toByteArray();

//write the bytes in file
        FileOutputStream fos = new FileOutputStream(f);
        fos.write(bitmapdata);
        fos.flush();
        fos.close();
        return Uri.fromFile(f);
    }
}
