package GLES.data.renderer;

/**
 * Created by Oni on 12/9/2016.
 */

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.opengl.EGL14;
import android.opengl.GLSurfaceView;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;


import android.opengl.EGLContext;
import android.opengl.EGLDisplay;
import android.opengl.EGLSurface;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGL10;
import GLES.data.Square;
import GLES.data.utils.ImageUtil;
import GLES.data.utils.SurfaceRecorder;
import we.orca.oni.orcaphotowatereffect.MainActivity;
import we.orca.oni.orcaphotowatereffect.R;
import we.orca.oni.orcaphotowatereffect.ResultViewActivity;

import static android.opengl.GLES20.*;
import static android.opengl.GLUtils.*;

/**
 * Created by phind on 11/30/16.
 */
public class ImageRenderer implements GLSurfaceView.Renderer,MediaScannerConnection.MediaScannerConnectionClient {

    private int program;
    private Context context;
    private Bitmap photo;
    private int photoWidth, photoHeight;
    private int textures[] = new int[2];
    private Square square;
    private int mode;
    private int shaderResource;
    private float aspectRef = 0f;
    private int width;
    private int height;
    private Bitmap captureImage; //export bitmap from glsurface
    private boolean isCapturing = false; //Is capture?
    private EGLDisplay mSavedEglDisplay;
    private EGLSurface mSavedEglDrawSurface;
    private EGLSurface mSavedEglReadSurface;
    private EGLContext mSavedEglContext;
    private boolean isRecordingS=false;
    private ProgressDialog pd;
    private MediaScannerConnection mCon;
    private String finalPath;

    private int mFrameCount;

    public ImageRenderer(Context ctx, Bitmap texture, int mode, ProgressDialog pd) {
        super();
        context = ctx;
        photo = texture;
        this.mode = mode;
        photoHeight = photo.getHeight();
        photoWidth = photo.getWidth();
        this.pd = pd;
    }

    public ImageRenderer(Context ctx, Bitmap texture, int mode, ProgressDialog pd, float as) {
        super();
        context = ctx;
        photo = texture;
        this.mode = mode;
        photoHeight = photo.getHeight();
        photoWidth = photo.getWidth();
        aspectRef = as;
        this.pd = pd;
    }

    private void generateSquare() {
        glGenTextures(2, textures, 0);
        glBindTexture(GL_TEXTURE_2D, textures[0]);
        Log.i("MyOpenGL", "Texture id: " + textures[0]);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        switch (mode) {
            case MainActivity.WATER_RIPPLE:
                shaderResource = R.raw.texture_fragment_shader;
                break;
            case MainActivity.WATER_REFLECTION:
                shaderResource = R.raw.reflection_fragment_shader;
                break;
            case MainActivity.WATER_WAVE:
                shaderResource = R.raw.waves_fragment_shader;
                break;
        }
        texImage2D(GL_TEXTURE_2D, 0, photo, 0);
        if (mode == MainActivity.WATER_REFLECTION)
            square = new Square(context, shaderResource, aspectRef);
        else
            square = new Square(context, shaderResource);
    }

    /**
     * Called when the surface is created or recreated.
     * <p/>
     * Called when the rendering thread
     * starts and whenever the EGL context is lost. The EGL context will typically
     * be lost when the Android device awakes after going to sleep.
     * <p/>
     * Since this method is called at the beginning of rendering, as well as
     * every time the EGL context is lost, this method is a convenient place to put
     * code to create resources that need to be created when the rendering
     * starts, and that need to be recreated when the EGL context is lost.
     * Textures are an example of a resource that you might want to create
     * here.
     * <p/>
     * Note that when the EGL context is lost, all OpenGL resources associated
     * with that context will be automatically deleted. You do not need to call
     * the corresponding "glDelete" methods such as glDeleteTextures to
     * manually delete these lost resources.
     * <p/>
     *
     * @param gl10      the GL interface. Use <code>instanceof</code> to
     *                  test if the interface supports GL11 or higher interfaces.
     * @param eglConfig the EGLConfig of the created surface. Can be used
     */
    @Override
    public void onSurfaceCreated(GL10 gl10, javax.microedition.khronos.egl.EGLConfig eglConfig) {
        glClearColor(1.0f, 0.0f, 0.0f, 0.0f);
        SurfaceRecorder recorder = SurfaceRecorder.getInstance();
        if (recorder.isRecording()) {
            saveRenderState();
            recorder.firstTimeSetup();
            recorder.makeCurrent();
            restoreRenderState();

            mFrameCount = 0;
        }
    }

    /**
     * Called when the surface changed size.
     * <p/>
     * Called after the surface is created and whenever
     * the OpenGL ES surface size changes.
     * <p/>
     * Typically you will set your viewport here. If your camera
     * is fixed then you could also set your projection matrix here:
     * <pre class="prettyprint">
     * void onSurfaceChanged(GL10 gl, int width, int height) {
     * gl.glViewport(0, 0, width, height);
     * // for a fixed camera, set the projection too
     * float ratio = (float) width / height;
     * gl.glMatrixMode(GL10.GL_PROJECTION);
     * gl.glLoadIdentity();
     * gl.glFrustumf(-ratio, ratio, -1, 1, 1, 10);
     * }
     * </pre>
     *
     * @param gl     the GL interface. Use <code>instanceof</code> to
     *               test if the interface supports GL11 or higher interfaces.
     * @param width
     * @param height
     */
    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        glViewport(0, 0, width, height);
        Log.i("IMAGE RENDERER",width+" : "+height);
        this.width = width;
        this.height = height;
        //gl.glMatrixMode(GL10.GL_PROJECTION);
        generateSquare();
/*        final float aspectRatio = width>height?
                (float)width/(float)height:(float)height/(float)width;
        if(width>height){
            orthoM(square.projectionMatrix,0,-aspectRatio,aspectRatio,-1f,1f,-1f,1f);
        }else{
            orthoM(square.projectionMatrix,0,-1f,1f,-aspectRatio,aspectRatio,-1f,1f);
        }*/
        glClearColor(1, 0, 0, 1);

    }

    /**
     * Called to draw the current frame.
     * <p/>
     * This method is responsible for drawing the current frame.
     * <p/>
     * The implementation of this method typically looks like this:
     * <pre class="prettyprint">
     * void onDrawFrame(GL10 gl) {
     * gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);
     * //... other gl calls to render the scene ...
     * }
     * </pre>
     *
     * @param gl the GL interface. Use <code>instanceof</code> to
     *           test if the interface supports GL11 or higher interfaces.
     */
    @Override
    public void onDrawFrame(GL10 gl) {
        square.draw(textures[0]);
        SurfaceRecorder recorder = SurfaceRecorder.getInstance();
        if (isRecordingS && recorder.isRecording() && recordThisFrame()) {
            saveRenderState();

            // switch to recorder state
            recorder.makeCurrent();
            recorder.setViewport();

            // render everything again
            square.draw(textures[0]);
            recorder.swapBuffers();
            restoreRenderState();
        }

        if (isCapturing) {
            captureBitmap();
        }
    }

    public void setTouchPoint(float x, float y) {
        square.addRip(x, y);
    }

    /*
   THis section is use for catpure image from glsurfaceview
   Begin
    */
    private void captureBitmap() {
        EGL10 egl = (EGL10) javax.microedition.khronos.egl.EGLContext.getEGL();
        GL10 gl = (GL10) egl.eglGetCurrentContext().getGL();
        captureImage = ImageUtil.createBitmapFromGLSurface(0, 0, this.width, this.height, gl);
        //photo.recycle();
        isCapturing = false;
        try {
            //Write file
            String filename = ImageUtil.filenameGen(context) + ".png";
            File file = new File(Environment.getExternalStorageDirectory(), "/" + filename);
            FileOutputStream stream = new FileOutputStream(file);
            captureImage.compress(Bitmap.CompressFormat.PNG, 100, stream);

            //Cleanup
            stream.close();
            captureImage.recycle();

            //Pop intent
            Intent in1 = new Intent(context, ResultViewActivity.class);
            in1.putExtra("link", filename);
            in1.putExtra("type",ImageUtil.IMAGE_TYPE);
            finalPath = file.getAbsolutePath();
            mCon = new MediaScannerConnection(context,this);
            mCon.connect();

            /*MediaScannerConnection.scanFile(context,
                    new String[]{file.getAbsolutePath()}, null,
                    new MediaScannerConnection.OnScanCompletedListener() {
                        public void onScanCompleted(String path, Uri uri) {
                            //now visible in gallery
                            pd.dismiss();

                        }
                    }
            );*/
            in1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            context.startActivity(in1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /*
    end
     */

    //setter for isCapture
    public void setIsCapture(boolean isCapture) {
        this.isCapturing = isCapture;
    }

    public void setIsRecording(boolean isRecord){
        this.isRecordingS = isRecord;
    }

    private void saveRenderState() {
        mSavedEglDisplay = EGL14.eglGetCurrentDisplay();
        mSavedEglDrawSurface = EGL14.eglGetCurrentSurface(EGL14.EGL_DRAW);
        mSavedEglReadSurface = EGL14.eglGetCurrentSurface(EGL14.EGL_READ);
        mSavedEglContext = EGL14.eglGetCurrentContext();
    }

    /**
     * +     * Saves the current projection matrix and EGL state.
     * +
     */
    private void restoreRenderState() {
        // switch back to previous state
        if (!EGL14.eglMakeCurrent(mSavedEglDisplay, mSavedEglDrawSurface, mSavedEglReadSurface,
                mSavedEglContext)) {
            throw new RuntimeException("eglMakeCurrent failed");
        }
    }

    private boolean recordThisFrame() {
        final int TARGET_FPS = 30;

        mFrameCount++;
        switch (TARGET_FPS) {
            case 60:
                return true;
            case 30:
                return (mFrameCount & 0x01) == 0;
            case 24:
                // want 2 out of every 5 frames
                int mod = mFrameCount % 5;
                return mod == 0 || mod == 2;
            default:
                return true;
        }
    }

    public void onViewStop(){
        isRecordingS = false;
        SurfaceRecorder.getInstance().gamePaused();
    }

    @Override
    public void onMediaScannerConnected() {
        mCon.scanFile(finalPath, "image/png");
    }

    @Override
    public void onScanCompleted(String s, Uri uri) {
        pd.dismiss();
        mCon.disconnect();
        //context.unbindService(mCon);
    }

    public void setX(int x){
        square.setX(x);
    }

    public void setY(int y){
        square.setY(y);
    }
}
