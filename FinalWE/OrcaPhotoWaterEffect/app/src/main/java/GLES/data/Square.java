package GLES.data;

import android.content.Context;
import android.graphics.Bitmap;
import android.opengl.GLES20;
import android.util.Log;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.opengles.GL10;

import GLES.data.utils.ImageUtil;
import GLES.data.utils.TextResourceReader;
import we.orca.oni.orcaphotowatereffect.R;

import static android.opengl.GLES20.*;
import static android.opengl.GLUtils.*;
import static android.opengl.Matrix.*;

/**
 * Created by Oni on 12/9/2016.
 */
public class Square {
    private long startTime;
    private long eslap;
    private int MAX_RIP = 5;
    private int vertexShader;
    private int fragmentShader;
    private int program;
    private int uTimeLocation;
    private int uXExpose;
    private int uYExpose;
    private Context context;
    private static final String U_MATRIX = "u_Matrix";
    public final float[] projectionMatrix = new float[16];
    private int uMaxtrixLocation;
    private int fragmentShaderResource;
    private float aspectRef;
    private int touchXLocation;
    private int touchYLocation;
    private float xTouch=0.5f;
    private float yTouch=0.5f;
    private int indexRip;
    private float xA = 0.005f;
    private float yA = 0.05f;

    public Square(Context ctx, int fragmentShaderResource) {
        context = ctx;
        startTime = System.currentTimeMillis();
        indexRip = 0;
        this.fragmentShaderResource = fragmentShaderResource;
        if(fragmentShaderResource == R.raw.texture_fragment_shader)
        {
            setX(20);
            setY(20);
        }
        initalizeBuffers();
        initializeProgram();
    }

    public Square(Context ctx, int fragmentShaderResource, float aspect) {
        context = ctx;
        startTime = System.currentTimeMillis();
        indexRip = 0;
        aspectRef=aspect;

        this.fragmentShaderResource = fragmentShaderResource;
        initalizeBuffers();
        initializeProgram();
    }

    private float vertices[] = {
            -1f, -1f,
            1f, -1f,
            -1f, 1f,
            1f, 1f
    };

    private float textureVerticates[] = {
            0f, 1f,
            1f, 1f,
            0f, 0f,
            1f, 0f
    };

    private FloatBuffer verticesBuffer;
    private FloatBuffer textureBuffer;

    private void initalizeBuffers() {
        ByteBuffer buf = ByteBuffer.allocateDirect(vertices.length * 4);
        buf.order(ByteOrder.nativeOrder());
        verticesBuffer = buf.asFloatBuffer();
        verticesBuffer.put(vertices);
        verticesBuffer.position(0);

        buf = ByteBuffer.allocateDirect(textureVerticates.length * 4);
        buf.order(ByteOrder.nativeOrder());
        textureBuffer = buf.asFloatBuffer();
        textureBuffer.put(textureVerticates);
        textureBuffer.position(0);
    }

    private void initializeProgram() {
        //get mode
        vertexShader = glCreateShader(GLES20.GL_VERTEX_SHADER);
        glShaderSource(vertexShader, TextResourceReader.readTextFileFromResource(context, R.raw.texture_vertex_shader));
        glCompileShader(vertexShader);
        Log.i("MyOpenGL", "Vertex: " + vertexShader);
        fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
        glShaderSource(fragmentShader, TextResourceReader.readTextFileFromResource(context, fragmentShaderResource));
        glCompileShader(fragmentShader);
        Log.i("MyOpenGL", "Fragment: " + vertexShader);
        program = glCreateProgram();
        glAttachShader(program, vertexShader);
        glAttachShader(program, fragmentShader);
        Log.i("MyOpenGL", "Program: " + program);
        glLinkProgram(program);
    }

    public void draw(int texture) {
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        glUseProgram(program);
        glDisable(GL_BLEND);

        int positionHandle = glGetAttribLocation(program, "a_Position");
        int textureHandle = glGetUniformLocation(program, "u_TextureUnit");
        int textureHandle2 = glGetUniformLocation(program, "u_TextureUnit2");
        int texturePositionHandle = glGetAttribLocation(program, "a_TextureCoordinates");
        uTimeLocation = glGetUniformLocation(program, "u_Time");

        uMaxtrixLocation = glGetUniformLocation(program, U_MATRIX);
        glUniformMatrix4fv(uMaxtrixLocation, 1, false, projectionMatrix, 0);
        GLES20.glVertexAttribPointer(texturePositionHandle, 2, GLES20.GL_FLOAT, false, 0, textureBuffer);
        GLES20.glEnableVertexAttribArray(texturePositionHandle);

        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, texture);
        GLES20.glUniform1i(textureHandle, 0);
        GLES20.glVertexAttribPointer(positionHandle, 2, GLES20.GL_FLOAT, false, 0, verticesBuffer);
        GLES20.glEnableVertexAttribArray(positionHandle);

        eslap = System.currentTimeMillis() - startTime;
        if (eslap >= 30000) {
            startTime = System.currentTimeMillis();
        }
        if (fragmentShaderResource == R.raw.texture_fragment_shader) {
            touchXLocation = glGetUniformLocation(program, "u_xTouch");
            touchYLocation = glGetUniformLocation(program, "u_yTouch");
            glUniform1f(touchXLocation, xTouch);
            glUniform1f(touchYLocation, yTouch);

        }else
        {

            if (fragmentShaderResource == R.raw.reflection_fragment_shader) {
                //Log.i("MY_OPENGL_PROJ","aspect: "+aspectRef);
                int aspectLocation = glGetUniformLocation(program,"aspect");
                glUniform1f(aspectLocation,aspectRef);
            }
        }
        uXExpose = glGetUniformLocation(program,"u_x_expose");
        uYExpose = glGetUniformLocation(program,"u_y_expose");
        glUniform1f(uXExpose,xA);
        glUniform1f(uYExpose,yA);
        glUniform1f(uTimeLocation, (float) (eslap * 0.001));
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);
        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, 4);
    }

    public void addRip(float x, float y) {
        xTouch=x;
        yTouch=y;
    }

    public void setX(int x){
        xA = (float)x*0.001f;
    }

    public void setY(int y){
        yA = (float)y*0.001f;
    }
}
