precision mediump float;
uniform sampler2D u_TextureUnit;
uniform sampler2D u_TextureUnit2;
uniform float u_Time;
uniform float u_x_expose;
uniform float u_y_expose;
varying vec2 v_TextureCoordinates;

float radius = .5;

void main()
{
    float sepoffset = 0.005*cos(u_Time*3.0);
    //gl_FragColor = texture2D(u_TextureUnit, vec2(v_TextureCoordinates.x,1.2-v_TextureCoordinates.y));
    vec2 uv=v_TextureCoordinates;
    // Compute the mirror effect.
    float xoffset = u_x_expose*cos(u_Time*3.0+20.0*uv.y);
    //float yoffset = 0.05*(1.0+cos(iGlobalTime*3.0+50.0*uv.y));
    float yoffset = ((1.2 - uv.y)/1.2) * u_y_expose*(1.0+cos(u_Time*3.0+25.0*uv.y));
    vec4 color = texture2D(u_TextureUnit, vec2(uv.x+xoffset , (uv.y+ yoffset)));
    gl_FragColor = color;
}