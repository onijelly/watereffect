precision mediump float;
precision mediump int;
uniform sampler2D u_TextureUnit;
uniform sampler2D u_TextureUnit2;
uniform float u_Time;
uniform float u_xTouch;
uniform float u_yTouch;
varying vec2 v_TextureCoordinates;

float radius = 0.3f;

void main()
{
    vec2 centre = vec2(u_xTouch,u_yTouch);
    highp vec2 tc = v_TextureCoordinates.st;
    highp vec2 p = 1.5 * (tc-centre);
    highp float len = length(p);
    highp vec2 uv = fract(tc + (p/len)*10.0f*max(0.3, 2.-len)*cos(len*24.0-u_Time*4.0)*0.03);
    gl_FragColor = texture2D(u_TextureUnit,uv) ;
}