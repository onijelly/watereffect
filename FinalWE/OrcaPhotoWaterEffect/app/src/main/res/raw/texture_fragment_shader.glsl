precision mediump float;
uniform sampler2D u_TextureUnit;
uniform sampler2D u_TextureUnit2;
uniform float u_Time;
uniform float u_xTouch;
uniform float u_yTouch;
uniform float u_x_expose;
uniform float u_y_expose;
varying vec2 v_TextureCoordinates;

float radius = 0.1f;

void main()
{
    float t = clamp(u_Time/6.0,0.,1.);
    vec2 coords = v_TextureCoordinates.st;
    //vec2 is center point
    vec2 dir = coords - vec2(u_xTouch,u_yTouch);

    float dist = distance(coords, vec2(u_xTouch,u_yTouch));
        vec2 offset = dir*(sin(dist*4000.0*u_y_expose - u_Time*15.)+radius)/(u_x_expose*1000.);
        vec2 textCoord = coords + offset;
        vec4 diffuse = texture2D(u_TextureUnit,textCoord);
        vec4 mixin = texture2D(u_TextureUnit2,textCoord);

        gl_FragColor = mixin*t + diffuse*(1.-t);
    }