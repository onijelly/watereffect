precision mediump float;
uniform sampler2D u_TextureUnit;
uniform sampler2D u_TextureUnit2;
uniform float u_Time;
uniform float u_x_expose;
uniform float u_y_expose;
uniform float aspect;
varying vec2 v_TextureCoordinates;

float radius = .5;

void main()
{
    float sepoffset = 0.005*cos(u_Time*3.0);
    float compare = aspect;
    float pivot = compare*2.0f-0.1f;
    if(v_TextureCoordinates.y < compare)
    {
        gl_FragColor = texture2D(u_TextureUnit, vec2(v_TextureCoordinates.x,v_TextureCoordinates.y));
    }
    else
    {
        //gl_FragColor = texture2D(u_TextureUnit, vec2(v_TextureCoordinates.x,v_TextureCoordinates.y));
        vec2 uv=v_TextureCoordinates;
        // Compute the mirror effect.
        float xoffset = u_x_expose*cos(u_Time*3.0+200.0*uv.y); //defualt : 0.005
        //float yoffset = 0.05*(1.0+cos(iGlobalTime*3.0+50.0*uv.y));
        float yoffset = ((1.2f - uv.y)/1.2f) * u_y_expose*(1.0+cos(u_Time*3.0+50.0*uv.y)); //default: 0.05
        vec4 color = texture2D(u_TextureUnit, vec2(uv.x+xoffset , (pivot - uv.y+ yoffset)));
        gl_FragColor = color;
    }
}