package GLES.data.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.opengl.GLException;
import android.os.Build;
import android.provider.MediaStore;
import android.util.Log;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.IntBuffer;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.microedition.khronos.opengles.GL10;

/**
 * Created by Oni on 1/1/2017.
 */
public class ImageUtil {
    public static final String FILE_PRE="ORCA_WE";
    public static final String VIDEO_PRE="ORCA_WE_VIDEO";
    public static final int VIDEO_TYPE = 2;
    public static final int IMAGE_TYPE = 1;
    public static Bitmap createBitmapFromGLSurface(int x, int y, int w, int h, GL10 gl)
            throws OutOfMemoryError {
        int bitmapBuffer[] = new int[w * h];
        int bitmapSource[] = new int[w * h];
        IntBuffer intBuffer = IntBuffer.wrap(bitmapBuffer);
        intBuffer.position(0);

        try {
            gl.glReadPixels(x, y, w, h, GL10.GL_RGBA, GL10.GL_UNSIGNED_BYTE, intBuffer);
            int offset1, offset2;
            for (int i = 0; i < h; i++) {
                offset1 = i * w;
                offset2 = (h - i - 1) * w;
                for (int j = 0; j < w; j++) {
                    int texturePixel = bitmapBuffer[offset1 + j];
                    int blue = (texturePixel >> 16) & 0xff;
                    int red = (texturePixel << 16) & 0x00ff0000;
                    int pixel = (texturePixel & 0xff00ff00) | red | blue;
                    bitmapSource[offset2 + j] = pixel;
                }
            }
        } catch (GLException e) {
            return null;
        }

        return Bitmap.createBitmap(bitmapSource, w, h, Bitmap.Config.ARGB_8888);
    }

    public static String filenameGen(Context context){
        StringBuilder result =new StringBuilder(ImageUtil.FILE_PRE);
        SharedPreferences settings = context.getSharedPreferences(ImageUtil.FILE_PRE, 0);
        int no = settings.getInt("Number", 1);

        /*get current date*/
        SimpleDateFormat dfDate  = new SimpleDateFormat("ddMMMyyyy");
        String seconds="";
        Calendar c = Calendar.getInstance();
        seconds=dfDate.format(c.getTime());
        /*get saved date*/
        String oldSec=settings.getString("date", seconds);
        if(oldSec.equals(seconds)){
            no+=1;
        }else{
            no=1;
        }
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt("Number",no);
        editor.putString("date", seconds);
        editor.commit();
        result.append(seconds).append(no);
        Log.i("FILE NAME",result.toString());
        return result.toString();
    }

    public static String videoFileNameGen(Context context){
        StringBuilder result =new StringBuilder(ImageUtil.VIDEO_PRE);
        SharedPreferences settings = context.getSharedPreferences(ImageUtil.VIDEO_PRE, 0);
        int no = settings.getInt("VideoNumber", 1);

        /*get current date*/
        SimpleDateFormat dfDate  = new SimpleDateFormat("ddMMMyyyy");
        String seconds="";
        Calendar c = Calendar.getInstance();
        seconds=dfDate.format(c.getTime());
        /*get saved date*/
        String oldSec=settings.getString("VideoDate", seconds);
        if(oldSec.equals(seconds)){
            no+=1;
        }else{
            no=1;
        }
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt("VideoNumber",no);
        editor.putString("VideoDate", seconds);
        editor.commit();
        result.append(seconds).append(no);
        Log.i("FILE NAME",result.toString());
        return result.toString();
    }


    public static Bitmap rotateBitmap(String src, Bitmap bitmap) {
        try {
            int orientation = getExifOrientation(src);
            if (orientation == 1) {
                return bitmap;
            }
            Log.i("ROTATION","ROTATION: "+orientation);
            Matrix matrix = new Matrix();
            switch (orientation) {
                case 2:
                    matrix.setScale(-1, 1);
                    break;
                case 3:
                    matrix.setRotate(180);
                    break;
                case 4:
                    matrix.setRotate(180);
                    matrix.postScale(-1, 1);
                    break;
                case 5:
                    matrix.setRotate(90);
                    matrix.postScale(-1, 1);
                    break;
                case 6:
                    matrix.setRotate(90);
                    break;
                case 7:
                    matrix.setRotate(-90);
                    matrix.postScale(-1, 1);
                    break;
                case 8:
                    matrix.setRotate(-90);
                    break;
                default:
                    return bitmap;
            }

            try {
                Bitmap oriented = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                bitmap.recycle();
                return oriented;
            } catch (OutOfMemoryError e) {
                e.printStackTrace();
                return bitmap;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return bitmap;
    }

    private static int getExifOrientation(String src) throws IOException {
        int orientation = 1;
        try {
            if (Build.VERSION.SDK_INT >= 5) {
                Class<?> exifClass = Class.forName("android.media.ExifInterface");
                Constructor<?> exifConstructor = exifClass.getConstructor(new Class[] { String.class });
                Object exifInstance = exifConstructor.newInstance(new Object[] { src });
                Method getAttributeInt = exifClass.getMethod("getAttributeInt", new Class[] { String.class, int.class });
                Field tagOrientationField = exifClass.getField("TAG_ORIENTATION");
                String tagOrientation = (String) tagOrientationField.get(null);
                orientation = (Integer) getAttributeInt.invoke(exifInstance, new Object[] { tagOrientation, 1});
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        return orientation;
    }

    public static  String getRealImagePath(Uri uri,Context context){
        String result;
        Cursor cursor = context.getContentResolver().query(uri, null, null, null,null);
        if(cursor == null){
            result = uri.getPath();
        }else{
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }
}
