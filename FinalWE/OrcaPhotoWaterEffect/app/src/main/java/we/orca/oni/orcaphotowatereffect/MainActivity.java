package we.orca.oni.orcaphotowatereffect;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.mvc.imagepicker.ImagePicker;
import com.soundcloud.android.crop.Crop;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private Button btnWaterRipple;
    private Button btnWaterWave;
    private Button btnWaterReflection;
    private static final int PICK_IMAGE_REQUEST = 1;
    private Bitmap bitmapImage;
    private int effectType;
    public static final int WATER_REFLECTION = 1;
    public static final int WATER_RIPPLE = 2;
    public static final int WATER_WAVE = 3;
    private static final int PERMISSION_REQUEST_CODE=100;
    private float aspectCrop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        MobileAds.initialize(getApplicationContext(), "ca-app-pub-9647322085691192~2072890668");
        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().addTestDevice("85C5AD1E233AC79C72C86419A91B71CC").build();

        mAdView.loadAd(adRequest);
        mAdView = (AdView) findViewById(R.id.adView2);
        mAdView.loadAd(adRequest);

        btnWaterReflection = (Button) findViewById(R.id.btn_water_reflect);
        btnWaterWave = (Button) findViewById(R.id.btn_water_wave);
        btnWaterRipple = (Button) findViewById(R.id.btn_water_ripple);
        if (Build.VERSION.SDK_INT >= 23)
        {
            if (checkPermission())
            {
                // Code for above or equal 23 API Oriented Device
                // Create a common Method for both
            } else {
                requestPermission();
            }
        }
        btnWaterRipple.setOnClickListener(this);
        btnWaterWave.setOnClickListener(this);
        btnWaterReflection.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_water_reflect:
                effectType = WATER_REFLECTION;
                openImagePicker();
                break;
            case R.id.btn_water_ripple:
                effectType = WATER_RIPPLE;
                openImagePicker();
                break;
            case R.id.btn_water_wave:
                effectType = WATER_WAVE;
                openImagePicker();
                break;
        }
    }

    public void openImagePicker() {
        ImagePicker.pickImage(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.i("RETURN CODE","Request code: "+requestCode);
        if (requestCode == ImagePicker.PICK_IMAGE_REQUEST_CODE&& resultCode == RESULT_OK && data != null && data.getData() != null) {
            Intent i=new Intent(this, MainDrawScreen.class);
            if(effectType == WATER_REFLECTION){
                DisplayMetrics displaymetrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
                int height = (displaymetrics.heightPixels-50)/2;
                int width = displaymetrics.widthPixels;
                final BitmapFactory.Options options = new BitmapFactory.Options();
                options.inScaled = false;
                Uri templeUri=null;
                    Bitmap temp = ImagePicker.getImageFromResult(this, requestCode, resultCode, data);
                    //temp = MainDrawScreen.rotateBitmap(temp,90);
                    Uri uri=getImageUri(this, temp);
                    Log.i("IMAGE URI",uri.toString());
                    //Uri destination = Uri.fromFile(new File(getCacheDir(), "cropped"));
                    //Crop.of(uri,destination).withMaxSize(width,height).start(this);
                    CropImage.activity(uri)
                            .setGuidelines(CropImageView.Guidelines.ON)
                            .setMaxCropResultSize(width,height)
                            .setRequestedSize(width,height)
                            .start(this);
            }else {
                Bitmap bitmap = ImagePicker.getImageFromResult(this, requestCode, resultCode, data);
                Uri uri = getImageUri(this,bitmap);
                i.putExtra("mode", effectType);
                i.putExtra("texture", uri);
                startActivity(i);
            }
        }else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            Intent i=new Intent(this, MainDrawScreen.class);
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            Uri uri = result.getUri();
            try {
                Bitmap cropped = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
                i.putExtra("mode", effectType);
                i.putExtra("texture", afterCrop(cropped));
                i.putExtra("aspect",aspectCrop);
                startActivity(i);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(MainActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Toast.makeText(MainActivity.this, "Write External Storage permission allows us to do store images. Please allow this permission in App Settings.", Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e("value", "Permission Granted, Now you can use local drive .");
                } else {
                    Log.e("value", "Permission Denied, You cannot use local drive .");
                }
                break;
        }
    }

    private Uri afterCrop(Bitmap bmp){
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        int width = displaymetrics.widthPixels;

        Bitmap temp = Bitmap.createBitmap(width,height, Bitmap.Config.ARGB_8888);

        float ratio = (float)width/(float)bmp.getWidth();
        int scaleHeight = (int) (ratio*bmp.getHeight());
        bmp = Bitmap.createScaledBitmap(bmp,width,scaleHeight,false);
        aspectCrop=(float)bmp.getHeight()/(float)height;
        Log.i("IMAGE CROPPED","Before: "+bmp.getHeight()+" - After: "+height);
        /*for(int i=0; i<temp.getWidth(); i++){
            for(int j=0; j<temp.getHeight(); j++){
                temp.setPixel(i,j, Color.BLACK);
            }
        }*/
        for(int i=0; i<bmp.getWidth();i++){
            for(int j=0; j<bmp.getHeight();j++){
                temp.setPixel(i,j,bmp.getPixel(i,j));
            }
        }
        ByteArrayOutputStream  outStream = null;
        File file = new File(Environment.getExternalStorageDirectory().toString(), "aftercrop");
        file.delete();
        bmp.recycle();
        try {
            outStream = new ByteArrayOutputStream ();
            temp.compress(Bitmap.CompressFormat.PNG, 100, outStream);
            FileOutputStream fo = new FileOutputStream(file,true);
            fo.write(outStream.toByteArray());
            // remember close file output
            fo.close();
            outStream.flush();
            outStream.close();
            return Uri.fromFile(file);
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
