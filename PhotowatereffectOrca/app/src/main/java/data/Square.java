package data;

import android.content.Context;
import android.opengl.GLES20;
import android.util.Log;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import pwe.orca.com.oni.photowatereffectorca.R;
import utils.TextResourceReader;

import static android.opengl.GLES20.*;
import static android.opengl.GLUtils.*;
import static android.opengl.Matrix.*;

/**
 * Created by Oni on 12/1/2016.
 */
public class Square {

    private long startTime;
    private long eslap;

    private int vertexShader;
    private int fragmentShader;
    private int program;
    private int uTimeLocation;
    private Context context;
    private static final String U_MATRIX = "u_Matrix";
    public final float[] projectionMatrix = new float[16];
    private int uMaxtrixLocation;
    private int touchXLocation;
    private int touchYLocation;
    private float xTouch[];
    private float yTouch[];
    private int indexRip;
    public Square(Context ctx){
        context=ctx;
        startTime = System.currentTimeMillis();
        initalizeBuffers();
        initializeProgram();
    }

    private float vertices[]={
            -1f, -1f,
            1f, -1f,
            -1f, 1f,
            1f, 1f
    };

    private float textureVerticates[]={
            0f, 1f,
            1f, 1f,
            0f, 0f,
            1f, 0f
    };

    private FloatBuffer verticesBuffer;
    private FloatBuffer textureBuffer;

    private void initalizeBuffers(){
        ByteBuffer buf = ByteBuffer.allocateDirect(vertices.length*4);
        buf.order(ByteOrder.nativeOrder());
        verticesBuffer = buf.asFloatBuffer();
        verticesBuffer.put(vertices);
        verticesBuffer.position(0);

        buf = ByteBuffer.allocateDirect(textureVerticates.length*4);
        buf.order(ByteOrder.nativeOrder());
        textureBuffer = buf.asFloatBuffer();
        textureBuffer.put(textureVerticates);
        textureBuffer.position(0);
    }

    private void initializeProgram(){
        vertexShader = glCreateShader(GLES20.GL_VERTEX_SHADER);
        glShaderSource(vertexShader, TextResourceReader.readTextFileFromResource(context,R.raw.texture_vertex_shader));
        glCompileShader(vertexShader);
        Log.i("MyOpenGL","Vertex: "+vertexShader);
        fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
        glShaderSource(fragmentShader,TextResourceReader.readTextFileFromResource(context,R.raw.texture_fragment_shader));
        glCompileShader(fragmentShader);
        Log.i("MyOpenGL","Fragment: "+vertexShader);
        program=glCreateProgram();
        glAttachShader(program, vertexShader);
        glAttachShader(program, fragmentShader);
        Log.i("MyOpenGL","Program: "+program);
        glLinkProgram(program);
    }

    public void draw(int texture){
        glBindFramebuffer(GL_FRAMEBUFFER,0);
        glUseProgram(program);
        glDisable(GL_BLEND);

        int positionHandle = glGetAttribLocation(program, "a_Position");
        int textureHandle = glGetUniformLocation(program, "u_TextureUnit");
        int textureHandle2 = glGetUniformLocation(program,"u_TextureUnit2");
        int texturePositionHandle = glGetAttribLocation(program,"a_TextureCoordinates");
        touchXLocation = glGetUniformLocation(program,"u_xTouch");
        touchYLocation = glGetUniformLocation(program,"u_yTouch");

        uTimeLocation = glGetUniformLocation(program,"u_Time");
        uMaxtrixLocation=glGetUniformLocation(program,U_MATRIX);
        glUniformMatrix4fv(uMaxtrixLocation,1,false, projectionMatrix,0);
        GLES20.glVertexAttribPointer(texturePositionHandle, 2, GLES20.GL_FLOAT, false, 0, textureBuffer);
        GLES20.glEnableVertexAttribArray(texturePositionHandle);

        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, texture);
        GLES20.glUniform1i(textureHandle, 0);
        GLES20.glVertexAttribPointer(positionHandle, 2, GLES20.GL_FLOAT, false, 0, verticesBuffer);
        GLES20.glEnableVertexAttribArray(positionHandle);

        eslap = System.currentTimeMillis()-startTime;
        if(eslap >=30000){
            startTime = System.currentTimeMillis();
        }
        if(xTouch!=0) {
            touchXLocation = glGetUniformLocation(program, "u_xTouch");
            touchYLocation = glGetUniformLocation(program, "u_yTouch");
            glUniform1fv(touchXLocation, MAX_RIP, xTouch, 0);
            glUniform1fv(touchYLocation, MAX_RIP, yTouch, 0);
            glUniform1f(uTimeLocation, (float) (eslap * 0.001));
        }
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);
        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, 4);

    }

    public void addRip(float x, float y) {
        indexRip = indexRip % MAX_RIP;
        xTouch[indexRip] = x;
        yTouch[indexRip] = -y;
        Log.i("TOUCH POINT", "Index: "+indexRip+"x: " + xTouch[indexRip] + "- y: " + yTouch[indexRip]);
        indexRip = indexRip + 1;
    }
}