package pwe.orca.com.oni.photowatereffectorca;

import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.ConfigurationInfo;
import android.graphics.PixelFormat;
import android.opengl.GLSurfaceView;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private GLSurfaceView glSurfaceView;
    private boolean rendered = false;
    private ImageRenderer imageRenderer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        glSurfaceView = (GLSurfaceView) findViewById(R.id.sv_image);
        setMotionEvent();
        final ActivityManager activityManager =
                (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        final ConfigurationInfo configurationInfo =
                activityManager.getDeviceConfigurationInfo();
        final boolean supportsEs2 = configurationInfo.reqGlEsVersion >= 0x20000;

        if (supportsEs2) {
            // Request an OpenGL ES 2.0 compatible context.
            glSurfaceView.setEGLContextClientVersion(2);
            // Assign our renderer.
            imageRenderer = new ImageRenderer(this);
            glSurfaceView.setRenderer(imageRenderer);
            rendered = true;
        } else {
            Toast.makeText(this, "This device does not support OpenGL ES 2.0.",
                    Toast.LENGTH_LONG).show();
            return;
        }
    }

    private void setMotionEvent(){
        glSurfaceView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final float normalizedX =
                        (event.getX() / (float) v.getWidth()) * 2 - 1;
                final float normalizedY =
                        -((event.getY() / (float) v.getHeight()) * 2 - 1);
                glSurfaceView.queueEvent(new Runnable(){
                    @Override
                    public void run() {
                        imageRenderer.setTouchPoint(normalizedX,normalizedY);
                    }});

                switch (event.getAction()){
                    case MotionEvent.ACTION_DOWN:
                        Log.i("TOUCH POINT","x: "+normalizedX+"- y: "+normalizedY);
                        break;
                }
                return true;
            }
        });
    }
    /**
     * Dispatch onPause() to fragments.
     */
    @Override
    protected void onPause() {
        super.onPause();
        if(rendered){
            glSurfaceView.onPause();
        }
    }

    /**
     * Dispatch onResume() to fragments.  Note that for better inter-operation
     * with older versions of the platform, at the point of this call the
     * fragments attached to the activity are <em>not</em> resumed.  This means
     * that in some cases the previous state may still be saved, not allowing
     * fragment transactions that modify the state.  To correctly interact
     * with fragments in their proper state, you should instead override
     * {@link #onResumeFragments()}.
     */
    @Override
    protected void onResume() {
        super.onResume();
        if(rendered){
            glSurfaceView.onResume();
        }
    }
}
