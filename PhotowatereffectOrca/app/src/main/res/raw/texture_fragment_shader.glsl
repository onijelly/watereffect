precision mediump float;
uniform sampler2D u_TextureUnit;
uniform sampler2D u_TextureUnit2;
const int no=5;
uniform float u_Time;
uniform float u_xTouch[no];
uniform float u_yTouch[no];
varying vec2 v_TextureCoordinates;

float radius = .3;

void main()
{
    float t = clamp(u_Time/6.0,0.,1.);
    vec2 coords = v_TextureCoordinates;
    //vec2 is center point
    for(int i=0; i< no; i++){
        vec2 dir = coords - vec2(u_xTouch[i],u_yTouch[i]);
        float dist = distance(coords, vec2(u_xTouch[i],u_yTouch[i]));
        if(dist <= radius){
            vec2 offset = dir*(sin(dist*400.0 - u_Time*15.)+radius)/50.;
            vec2 textCoord = coords + offset;
            vec4 diffuse = texture2D(u_TextureUnit,textCoord);
            vec4 mixin = texture2D(u_TextureUnit2,textCoord);
            gl_FragColor = mixin*t + diffuse*(1.-t);
        }else{
            gl_FragColor = texture2D(u_TextureUnit,v_TextureCoordinates);
        }
    }

}