package we.orca.oni.orcaphotowatereffect;

import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.ConfigurationInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Camera;
import android.graphics.Matrix;
import android.media.Image;
import android.net.Uri;
import android.opengl.GLException;
import android.opengl.GLSurfaceView;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Toast;

import java.io.File;
import java.nio.IntBuffer;

import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.opengles.GL10;

import GLES.data.renderer.ImageRenderer;
import GLES.data.utils.ImageUtil;
import GLES.data.utils.SurfaceRecorder;

public class MainDrawScreen extends AppCompatActivity implements View.OnClickListener, SeekBar.OnSeekBarChangeListener {
    private GLSurfaceView glSurfaceView;
    private boolean rendered = false;
    private int mode;
    private float aspectRef;
    private ImageRenderer imageRenderer;
    private ImageButton imageBtn, videoBtn, editBtn;
    private LinearLayout lnBar;
    private SeekBar sbXAxis;
    private SeekBar sbYAxis;
    float height, width;
    private boolean clickEnabled;
    /*    protected String videoPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM) + File.separator;
        protected String lastFileName;
        private long startTime;*/
    private ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_draw_screen);
        imageBtn = (ImageButton) findViewById(R.id.btn_image);
        videoBtn = (ImageButton) findViewById(R.id.btn_video);
        editBtn = (ImageButton) findViewById(R.id.btn_edit);
        sbXAxis = (SeekBar) findViewById(R.id.seekbar_x);
        sbYAxis = (SeekBar) findViewById(R.id.seekbar_y);

        lnBar = (LinearLayout) findViewById(R.id.linear_edit_bar);
        lnBar.setVisibility(View.GONE);

        pd = new ProgressDialog(this);
        pd.setIndeterminate(true);
        pd.setCancelable(false);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        SurfaceRecorder.getInstance().prepareEncoder(this, pd);
        setButtonClick();
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        height = displaymetrics.heightPixels;
        width = displaymetrics.widthPixels;
        glSurfaceView = (GLSurfaceView) findViewById(R.id.sv_image);
        setMotionEvent();
        Uri uri = getIntent().getParcelableExtra("texture");
        mode = getIntent().getIntExtra("mode", 0);
        if (mode == MainActivity.WATER_REFLECTION) {
            aspectRef = getIntent().getFloatExtra("aspect", 0.6f);
        }
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = false;
        Bitmap bmp = null;
        try {
            bmp = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
            if (mode != MainActivity.WATER_REFLECTION) {
                String imagePath = uri.getPath();
                //Bitmap rezied = Bitmap.createScaledBitmap(bmp,720,1280,true);
                //bmp = ImageUtil.rotateBitmap(imagePath,bmp);
            }
            final ActivityManager activityManager =
                    (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
            final ConfigurationInfo configurationInfo =
                    activityManager.getDeviceConfigurationInfo();
            final boolean supportsEs2 = configurationInfo.reqGlEsVersion >= 0x20000;

            if (supportsEs2) {
                // Request an OpenGL ES 2.0 compatible context.
                glSurfaceView.setEGLContextClientVersion(2);
                // Assign our renderer.
                if (mode == MainActivity.WATER_REFLECTION)
                    imageRenderer = new ImageRenderer(this, bmp, mode, pd, aspectRef);
                else
                    imageRenderer = new ImageRenderer(this, bmp, mode, pd);
                glSurfaceView.setRenderer(imageRenderer);
                rendered = true;
            } else {
                Toast.makeText(this, "This device does not support OpenGL ES 2.0.",
                        Toast.LENGTH_LONG).show();
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Dispatch onPause() to fragments.
     */
    @Override
    protected void onPause() {
        super.onPause();
        if (rendered) {
            glSurfaceView.onPause();
        }
    }

    /**
     * Dispatch onResume() to fragments.  Note that for better inter-operation
     * with older versions of the platform, at the point of this call the
     * fragments attached to the activity are <em>not</em> resumed.  This means
     * that in some cases the previous state may still be saved, not allowing
     * fragment transactions that modify the state.  To correctly interact
     * with fragments in their proper state, you should instead override
     * {@link #onResumeFragments()}.
     */
    @Override
    protected void onResume() {
        super.onResume();
        if (rendered) {
            glSurfaceView.onResume();
        }
    }

    public static Bitmap rotateBitmap(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    private void setMotionEvent() {
        glSurfaceView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (clickEnabled) {
                    final float normalizedX =
                            (event.getX() / width) * 2 - 1;
                    final float normalizedY =
                            -((event.getY() / height) * 2 - 1);
                    Log.i("TOUCH POINT", "x: " + normalizedX + "- y: " + normalizedY);
                    glSurfaceView.queueEvent(new Runnable() {
                        @Override
                        public void run() {
                            imageRenderer.setTouchPoint(normalizedX, normalizedY);
                        }
                    });
                }
                return true;
            }
        });
    }

    private void setButtonClick() {
        imageBtn.setOnClickListener(this);
        videoBtn.setOnClickListener(this);
        editBtn.setOnClickListener(this);
        sbXAxis.setOnSeekBarChangeListener(this);
        sbYAxis.setOnSeekBarChangeListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_edit:
                toggleBar();
                break;
            case R.id.btn_image:
                pd.setTitle("Generate Image ...");
                pd.show();
                imageRenderer.setIsCapture(true);
                break;
            case R.id.btn_video:
                pd.setTitle("Generate Video ...");
                pd.show();
                imageRenderer.setIsRecording(true);
                new CountDownTimer(2000, 100) {

                    @Override
                    public void onTick(long l) {
                        //do nothing
                    }

                    @Override
                    public void onFinish() {
                        imageRenderer.onViewStop();
                    }
                }.start();
                break;
        }
    }

    public void toggleBar() {
        if (lnBar.getVisibility() == View.VISIBLE) {
            clickEnabled = true;
            lnBar.setVisibility(View.GONE);
            editBtn.setImageResource(R.drawable.ic_edit_24dp);
        } else {
            clickEnabled = false;
            lnBar.setVisibility(View.VISIBLE);
            editBtn.setImageResource(R.drawable.ic_edit_done);
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        int bar = seekBar.getId();
        switch (bar) {
            case R.id.seekbar_x:
                imageRenderer.setX(i);
                break;
            case R.id.seekbar_y:
                imageRenderer.setY(i);
                break;
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }
}
