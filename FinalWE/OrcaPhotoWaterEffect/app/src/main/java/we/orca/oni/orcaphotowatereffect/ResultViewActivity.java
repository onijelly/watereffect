package we.orca.oni.orcaphotowatereffect;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.VideoView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import java.io.File;
import java.io.FileInputStream;

import GLES.data.utils.ImageUtil;

public class ResultViewActivity extends AppCompatActivity implements View.OnClickListener {
    private ImageView iv_finalImage;
    private VideoView iv_finalVideo;
    private Bitmap finalImage;
    private Uri uri;
    private Button btnShare;
    private File imgFile;
    MediaController mediaController;
    private int type;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_view);
        MobileAds.initialize(getApplicationContext(), "ca-app-pub-9647322085691192~2072890668");
        AdView mAdView = (AdView) findViewById(R.id.adViewResult);
        AdRequest adRequest = new AdRequest.Builder().addTestDevice("85C5AD1E233AC79C72C86419A91B71CC").build();
        mAdView.loadAd(adRequest);
        iv_finalImage = (ImageView)findViewById(R.id.finalImage);
        iv_finalVideo = (VideoView)findViewById(R.id.finalVideo);
        btnShare = (Button)findViewById(R.id.btn_share);
        btnShare.setOnClickListener(this);
        mediaController= new MediaController(this);
        mediaController.setAnchorView(iv_finalVideo);


        Bitmap bmp = null;
        String filename = getIntent().getStringExtra("link");
        type = getIntent().getIntExtra("type",0);
        Log.i("MEDIA TYPE","media type: "+type);
        switch(type){
            case ImageUtil.IMAGE_TYPE:
                btnShare.setText("SHARE YOUR IMAGE");
                iv_finalImage.setVisibility(View.VISIBLE);
                iv_finalVideo.setVisibility(View.GONE);
                imgFile=new File(Environment.getExternalStorageDirectory(), "/"+filename);
                try {
                    FileInputStream is = new FileInputStream(imgFile);
                    bmp = BitmapFactory.decodeStream(is);
                    this.iv_finalImage.setImageBitmap(bmp);
                    is.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case ImageUtil.VIDEO_TYPE:
                btnShare.setText("SHARE YOUR VIDEO");
                iv_finalImage.setVisibility(View.GONE);
                iv_finalVideo.setVisibility(View.VISIBLE);
                iv_finalVideo.setBackgroundColor(Color.TRANSPARENT);
                uri = Uri.parse(Environment.getExternalStorageDirectory().getPath() + "/" + filename);
                Log.i("VIDEO URL", Environment.getExternalStorageDirectory().getPath() + "/" + filename);
                iv_finalVideo.setMediaController(mediaController);
                iv_finalVideo.setVideoURI(uri);
                iv_finalVideo.requestFocus();
                iv_finalVideo.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mediaPlayer) {
                        mediaPlayer.setLooping(true);
                        iv_finalVideo.setZOrderOnTop(false);
                        iv_finalVideo.start();
                    }
                });
                break;
        }

    }

    @Override
    public void onClick(View view) {
        final Intent shareIntent = new Intent(Intent.ACTION_SEND);
        if(type == ImageUtil.IMAGE_TYPE) {
            shareIntent.setType("image/png");
            shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(imgFile));
            startActivity(Intent.createChooser(shareIntent, "Share image using"));
        }else{
            shareIntent.setType("video/mp4");
            shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
            startActivity(Intent.createChooser(shareIntent, "Share video using"));
        }
    }
}
