package pwe.orca.com.oni.photowatereffectorca;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Shader;
import android.opengl.GLSurfaceView;
import android.util.Log;
import android.view.ContextThemeWrapper;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import data.Square;
import utils.ShaderHelper;
import utils.TextResourceReader;

import static android.opengl.GLES20.*;
import static android.opengl.GLUtils.*;
import static android.opengl.Matrix.*;
/**
 * Created by phind on 11/30/16.
 */
public class ImageRenderer implements GLSurfaceView.Renderer {

    private int program;
    private Context context;
    private Bitmap photo;
    private int photoWidth, photoHeight;
    private int textures[] = new int[2];
    private Square square;


    public ImageRenderer(Context ctx){
        super();
        context=ctx;
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = false;
        photo = BitmapFactory.decodeResource(context.getResources(), R.drawable.mabu, options);
        photoHeight=photo.getHeight();
        photoWidth=photo.getWidth();
    }

    private void generateSquare(){
        glGenTextures(2, textures, 0);
        glBindTexture(GL_TEXTURE_2D, textures[0]);
        Log.i("MyOpenGL","Texture id: "+textures[0]);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

        texImage2D(GL_TEXTURE_2D, 0, photo, 0);
        square = new Square(context);
    }

    /**
     * Called when the surface is created or recreated.
     * <p>
     * Called when the rendering thread
     * starts and whenever the EGL context is lost. The EGL context will typically
     * be lost when the Android device awakes after going to sleep.
     * <p>
     * Since this method is called at the beginning of rendering, as well as
     * every time the EGL context is lost, this method is a convenient place to put
     * code to create resources that need to be created when the rendering
     * starts, and that need to be recreated when the EGL context is lost.
     * Textures are an example of a resource that you might want to create
     * here.
     * <p>
     * Note that when the EGL context is lost, all OpenGL resources associated
     * with that context will be automatically deleted. You do not need to call
     * the corresponding "glDelete" methods such as glDeleteTextures to
     * manually delete these lost resources.
     * <p>
     *
     * @param gl     the GL interface. Use <code>instanceof</code> to
     *               test if the interface supports GL11 or higher interfaces.
     * @param config the EGLConfig of the created surface. Can be used
     */
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        glClearColor(1.0f, 0.0f, 0.0f, 0.0f);

    }

    /**
     * Called when the surface changed size.
     * <p>
     * Called after the surface is created and whenever
     * the OpenGL ES surface size changes.
     * <p>
     * Typically you will set your viewport here. If your camera
     * is fixed then you could also set your projection matrix here:
     * <pre class="prettyprint">
     * void onSurfaceChanged(GL10 gl, int width, int height) {
     * gl.glViewport(0, 0, width, height);
     * // for a fixed camera, set the projection too
     * float ratio = (float) width / height;
     * gl.glMatrixMode(GL10.GL_PROJECTION);
     * gl.glLoadIdentity();
     * gl.glFrustumf(-ratio, ratio, -1, 1, 1, 10);
     * }
     * </pre>
     *
     * @param gl     the GL interface. Use <code>instanceof</code> to
     *               test if the interface supports GL11 or higher interfaces.
     * @param width
     * @param height
     */
    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        glViewport(0,0,width,height);
        //gl.glMatrixMode(GL10.GL_PROJECTION);
        generateSquare();
        final float aspectRatio = width>height?
                (float)width/(float)height:(float)height/(float)width;
        if(width>height){
            orthoM(square.projectionMatrix,0,-aspectRatio,aspectRatio,-1f,1f,-1f,1f);
        }else{
            orthoM(square.projectionMatrix,0,-1f,1f,-aspectRatio,aspectRatio,-1f,1f);
        }
        glClearColor(1,0,0,1);

    }

    /**
     * Called to draw the current frame.
     * <p>
     * This method is responsible for drawing the current frame.
     * <p>
     * The implementation of this method typically looks like this:
     * <pre class="prettyprint">
     * void onDrawFrame(GL10 gl) {
     * gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);
     * //... other gl calls to render the scene ...
     * }
     * </pre>
     *
     * @param gl the GL interface. Use <code>instanceof</code> to
     *           test if the interface supports GL11 or higher interfaces.
     */
    @Override
    public void onDrawFrame(GL10 gl) {
        square.draw(textures[0]);
    }

    public void setTouchPoint(float x, float y){
        square.addRip(x,y);
    }
}
