precision mediump float;
uniform sampler2D u_TextureUnit;
uniform sampler2D u_TextureUnit2;
uniform float u_Time;
uniform float u_xTouch;
uniform float u_yTouch;
varying vec2 v_TextureCoordinates;

float radius = 0.1f;

void main()
{
    float t = clamp(u_Time/6.0,0.,1.);
    vec2 coords = v_TextureCoordinates.st;
    //vec2 is center point
    vec2 dir = coords - vec2(u_xTouch,u_yTouch);

    float dist = distance(coords, vec2(u_xTouch,u_yTouch));
    if(dist <= radius)
    {
        vec2 offset = dir*(sin(dist*500.0 - u_Time*15.)+radius)/10.;
        vec2 textCoord = fract(coords + offset);
        vec4 diffuse = texture2D(u_TextureUnit,textCoord);
        vec4 mixin = texture2D(u_TextureUnit2,textCoord);

        gl_FragColor = mixin*t + diffuse*(1.-t);
    }else{
        gl_FragColor = texture2D(u_TextureUnit,v_TextureCoordinates);
    }
/*    float sepoffset = 0.005*cos(u_Time*3.0);
    if(v_TextureCoordinates.y < 0.6)
    {
        gl_FragColor = texture2D(u_TextureUnit, vec2(v_TextureCoordinates.x,v_TextureCoordinates.y));
    }
    else
    {
        //gl_FragColor = texture2D(u_TextureUnit, vec2(v_TextureCoordinates.x,1.2-v_TextureCoordinates.y));
        vec2 uv=v_TextureCoordinates;
        // Compute the mirror effect.
        float xoffset = 0.005*cos(u_Time*3.0+200.0*uv.y);
        //float yoffset = 0.05*(1.0+cos(iGlobalTime*3.0+50.0*uv.y));
        float yoffset = ((1.2 - uv.y)/1.2) * 0.05*(1.0+cos(u_Time*3.0+50.0*uv.y));
        vec4 color = texture2D(u_TextureUnit, vec2(uv.x+xoffset , (1.2 - uv.y+ yoffset)));
        gl_FragColor = color;
    }*/
}