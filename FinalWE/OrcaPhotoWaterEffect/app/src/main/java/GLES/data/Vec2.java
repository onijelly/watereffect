package GLES.data;

/**
 * Created by Oni on 12/14/2016.
 */
public class Vec2 {
    public float X;
    public float Y;

    public Vec2(float x, float y){
        X=x;
        Y=y;
    }

    public void set(float x, float y){
        X=x;
        Y=y;
    }
}
